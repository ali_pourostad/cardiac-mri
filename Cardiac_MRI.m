cla; clc; clear;
load('sol_yxzt_pat18.mat');
xlim([0,20]);
f = figure(1);
a = [];
for i = 3
    subplot(2,2,[2,4]);
    cla;
    h = animatedline;
    for j = 1:20
        picture = reshape(sol_yxzt(:,:,i,j),256,256)./500;
        level = graythresh(picture);
        BW = imbinarize(picture,level);
        y = edge(picture, 'Canny', 0.3, 10);
        subplot(2,2,1);
        imshowpair(picture, J, 'blend');
        subplot(2,2,3);
        SE = strel('sphere', 3);
        J = imopen(y,SE);
        SE = strel('sphere', 10);
        y1 = imclose(y, SE);
        [B,L] = bwboundaries(y1,'noholes');
        imshow(label2rgb(L, @jet, [.5 .5 .5]))
        hold on
        C = [];
        I = 2;
        heart = B{I};
        kk = boundary(heart(:,2), heart(:,1));
        heart = [heart(kk,2), heart(kk,1)];
        
        subplot(2,2,1);
        hold on
        plot(heart(:,1), heart(:,2), 'r', 'LineWidth', 2);
        addpoints(h, j, polyarea(heart(:,2), heart(:,1)));
        drawnow;
    end
end